---
title: "The Great Gatsby BootCamp"
date: "2019-04-04"
---

Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptate similique pariatur molestias excepturi commodi voluptatem ipsum repellendus nam. Doloribus necessitatibus atque exercitationem harum ea consectetur esse quaerat odio iste reiciendis!

![Civic](./civic.jpg)

## Topics Covered

1. Gatsby
2. GraphQl
3. React