import React from 'react'
import Layout from '../components/layout'

const ContactPage = () => {
  return (
    <div>
      <Layout>
        <h1>Contact</h1>
        <p>Contact will show up here later on.</p>
        <p><a href="http://gitlab.com/marcoszillig" target="_blank" rel="noopener noreferrer">GitLab</a></p>
      </Layout>
    </div>
  )
}

export default ContactPage