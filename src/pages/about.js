import React from 'react'
import { Link } from 'gatsby'
import Layout from '../components/layout'

const AboutPage = () => {
  return (
    <div>
      <Layout>
        <h1>About Me</h1>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum tempore expedita praesentium harum sequi ipsa amet assumenda, excepturi sint at quisquam molestias deleniti possimus iure labore unde aspernatur nisi reiciendis.</p>
        <p>Need a developr? <Link to="/contact">Contact me.</Link></p>
      </Layout>
    </div>
  )
}

export default AboutPage